# my-yellow-pages

This is a mimimalist Node.js app, that shows how to implement your custom yellow pages server, in order to be used by an Istra platform. It uses  [Express](http://expressjs.com/).

Please refer to [./lib/yp.js](./lib/yp.js) to see the main logic, as well as the expected XML answers to use. A [Mocha](./test/test.js) test file will show the expected behavior for nominal and edge cases (400, 401, 403, 500, etc...)

# Quick start

Clone the repo:

    git clone git@gitlab.com:centile-pub/my-yellow-pages.git

Install the project dependencies:

    npm install
    
Launch the tests (and actually study them, since they show various nominal and edge cases for your own implementation):

    npm run test

Launch the my-yellow-pages server:

    npm run start

Alternatively, launch the server in debug mode, in order to connect a NodeJS debugger to it:

    npm run startd
    
Once launched, the server will run on http://localhost:5000 by default, and will answer to a "search" request:
    
    curl "http://localhost:5000/cgi-bin/gwi.exe/nn/xml?UserName=myLogin&Password=myPassword&Telephone=%252B33497231260"
    
And it will answer as well to the "get" request:

    curl "http://localhost:5000/cgi-bin/gwi.exe/nn/xml?UserName=myLogin&Password=myPassword&UniqueId=1234"

You may want to log the incoming HTTP requests by setting debugging traces on the server by setting the environmenet variable DEBUG to true:

    export DEBUG=true && npm run start
    
The variable is automatically set when using ``npm run startd``.

In order to override the PORT used by the server, you may use this launch command:

    export PORT=8888 && npm run start

# More info?
Please refer to Centile's extranet, in order to get the technical infomation about the feature, how to configure it, and how to implement your own yellow page server for your use case.

In case of doubt, ask us through the usual channels, or ultimately at https://centile.com/contact-page :-)
