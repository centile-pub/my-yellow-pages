// Our modules dependencies
const expect = require('Chai').expect;
const request = require('request');

// Our tested code dependencies
const server = require('../lib/server');
const yp = require('../lib/yp');

// Some constant
const PORT = process.env.PORT || 5000;
const CTYPE = 'text/xml; charset=utf-8';

describe('Test yp requests', () => {
  let child;

  before(() => {
    child = server.listen(PORT, () => {
      console.log(`> Listening on port ${PORT}`);
    });
  });

  after( () => {
    child.close();
  });

  const URL_ROOT = `http://localhost:${PORT}`;
  it('should return 400', (done) => {
    request.get(URL_ROOT, (err, res, body) => {
      expect(res.statusCode).to.equal(400);
      expect(res.body).to.equal('Bad Request');
      done();
    });
  });

  const URL_YP_NO_AUTH = `${URL_ROOT}/cgi-bin/gwi.exe/nn/xml`;
  it('should return 401', (done) => {
    request.get(URL_YP_NO_AUTH, (err, res, body) => {
      expect(res.statusCode).to.equal(401);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err4001);
      done();
    });
  });

  const URL_YP_BAD_AUTH = `${URL_YP_NO_AUTH}?UserName=NOTmyLogin&Password=NOTmyPassword`;
  it('should return 403', (done) => {
    request.get(URL_YP_BAD_AUTH, (err, res, body) => {
      expect(res.statusCode).to.equal(403);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err4003);
      done();
    });
  });

  const URL_YP_AUTH = `${URL_YP_NO_AUTH}?UserName=myLogin&Password=myPassword`;
  it('should return 400', (done) => {
    request.get(URL_YP_AUTH, (err, res, body) => {
      expect(res.statusCode).to.equal(400);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err4000);
      done();
    });
  });

  const URL_YP_AUTH_TEL_FOUND = `${URL_YP_AUTH}&Telephone=%252B33497231260`;
  it('should return 200', (done) => {
    request.get(URL_YP_AUTH_TEL_FOUND, (err, res, body) => {
      expect(res.statusCode).to.equal(200);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.search2000);
      done();
    });
  });

  const URL_YP_AUTH_TEL_TOOMANYRESULTS = `${URL_YP_AUTH}&Telephone=%252B33999999999`;
  it('should return 500', (done) => {
    request.get(URL_YP_AUTH_TEL_TOOMANYRESULTS, (err, res, body) => {
      expect(res.statusCode).to.equal(500);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err5000);
      done();
    });
  });

  const URL_YP_AUTH_TEL_NOTFOUND = `${URL_YP_AUTH}&Telephone=%252B33123456789`;
  it('should return 404', (done) => {
    request.get(URL_YP_AUTH_TEL_NOTFOUND, (err, res, body) => {
      expect(res.statusCode).to.equal(404);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err4004);
      done();
    });
  });

  const URL_YP_AUTH_GET_FOUND = `${URL_YP_AUTH}&UniqueId=1234`;
  it('should return 200', (done) => {
    request.get(URL_YP_AUTH_GET_FOUND, (err, res, body) => {
      expect(res.statusCode).to.equal(200);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.get2000);
      done();
    });
  });

  const URL_YP_AUTH_GET_NOTFOUND = `${URL_YP_AUTH}&UniqueId=WRONGID`;
  it('should return 404', (done) => {
    request.get(URL_YP_AUTH_TEL_NOTFOUND, (err, res, body) => {
      expect(res.statusCode).to.equal(404);
      expect(res.headers['content-type']).to.equal(CTYPE);
      expect(res.body).to.equal(yp.RESPONSES.err4004);
      done();
    });
  });
});
