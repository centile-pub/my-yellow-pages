// Our modules dependencies
const express = require('express');
const yp = require('./yp');

// Some constants
const e = express();

// A small helper function to transform a request into a loggable string
const logRequest = (r) => {
  if (process.env.DEBUG == "true") {
    console.log(`DEBUG: ${r.method} ${r.url} with headers:\n${JSON.stringify(r.headers, '\n', 4)}`);
  }
};

// Route yellow pages requests
e.get(
  '/cgi-bin/gwi.exe/nn/xml',
  (req, res) => {
    logRequest(req);
    yp.serve(req, res);
  });


// Anything else is an error
e.get('/*', (req, res) => {
  logRequest(req);
  res.sendStatus(400);
});

module.exports = e;
