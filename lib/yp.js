const TEXT_XML = 'text/xml';

const RESPONSES = {
  err4000: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="error">\n\
  <code>4000</code>\n\
  <string>Bad syntax.</string>\n\
</result>\n',

  err4001: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="error">\n\
  <code>4001</code>\n\
  <string>Unauthorized.</string>\n\
</result>\n',

  err4003: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="error">\n\
  <code>4003</code>\n\
  <string>Forbidden.</string>\n\
</result>\n',

  err4004: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="error">\n\
  <code>4004</code>\n\
  <string>Not found.</string>\n\
</result>\n',

  err5000: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="error">\n\
  <code>5000</code>\n\
  <string>An error occured: search results too large</string>\n\
</result>\n',

  search2000: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="List.Phone" howmany="1" Documents="1" Page="1/1">\n\
  <contact row="1">\n\
    <CLAS>MP</CLAS>\n\
    <RWID>1234</RWID>\n\
    <PHNU>0497231260</PHNU>\n\
    <ADDR>Les Aqueducs – Bât.4, 535 Route des Lucioles</ADDR>\n\
    <LOCA>06560 Sophia-Antipolis</LOCA>\n\
    <COCO>Sophia-Antipolis</COCO>\n\
  </contact>\n\
</result>\n',

  get2000: '<?xml version="1.0" encoding="ISO-8859-1"?>\n\
<result type="Doc.Get" howmany="1" Documents="1" Page="1/1">\n\
  <contact row="1">\n\
    <CLAS>MP</CLAS>\n\
    <RWID>1234</RWID>\n\
    <HILE>1</HILE>\n\
    <NALA>Centile Telecom Applications</NALA>\n\
    <PHNU>0497231260</PHNU>\n\
    <ADDR>Les Aqueducs – Bât.4, 535 Route des Lucioles</ADDR>\n\
    <LOCA>06560 Sophia-Antipolis</LOCA>\n\
    <COCO>Sophia-Antipolis</COCO>\n\
  </contact>\n\
</result>\n'
};

/*
 * FYI, Istra will perfrom requests such as
 *   GET /cgi-bin/gwi.exe/nn/xml?UserName=myLogin&Password=myPassword&Telephone=%252B33497231260&Limit=1
 * and:
 *   GET /cgi-bin/gwi.exe/nn/xml?UserName=myLogin&Password=myPassword&UniqueId=1234
 */

const serve = (req, res) => {
  if (req.query.UserName === undefined || req.query.Password === undefined) {
    // No credentials
    res.type(TEXT_XML);
    res.status(401);
    res.send(RESPONSES.err4001);
  } else if ("myLogin" !== req.query.UserName || "myPassword" !== req.query.Password) {
    // Incorrect credentials
    res.type(TEXT_XML);
    res.status(403);
    res.send(RESPONSES.err4003);
  } else if (req.query.Telephone !== undefined && req.query.Telephone !== null && req.query.Telephone.length > 0) {
    // Parse number to lookup (we don't mind the 'Limit' parameter: Istra always sends value 1)
    if ("%2B33497231260" === req.query.Telephone) {
      // Return Centile's match
      res.type(TEXT_XML);
      res.status(200);
      res.send(RESPONSES.search2000);
    } else if ("%2B33999999999" === req.query.Telephone) {
      // Return too many results
      res.type(TEXT_XML);
      res.status(500);
      res.send(RESPONSES.err5000);
    } else {
      // Returns not found
      res.type(TEXT_XML);
      res.status(404);
      res.send(RESPONSES.err4004);
    }
  } else if (req.query.UniqueId !== undefined && req.query.UniqueId !== null && req.query.UniqueId.length > 0) {
    // Parse UniqueId to look up
    if ("1234" === req.query.UniqueId) {
      // Return Centile's match
      res.type(TEXT_XML);
      res.status(200);
      res.send(RESPONSES.get2000);
    } else {
      // Returns not found
      res.type(TEXT_XML);
      res.status(404);
      res.send(RESPONSES.err4004);
    }
  } else {
    // No Telephone and no UniqueId parameter in query string leads to Bad Syntax
    res.type(TEXT_XML);
    res.status(400);
    res.send(RESPONSES.err4000);
  }
};

module.exports = {
  serve: serve,
  RESPONSES: RESPONSES
};
